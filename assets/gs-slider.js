/*
* selector  - parent selector for elements - (string) with dot
* prevClass - selector for previous arrow - (string) with dot
* nextClass - selector for next arrow - (string) with dot
* mobile    - enable only on mobile (boolean)
*/

class Slider {
    constructor(selector, prevClass, nextClass, mobile){
        var _ = this;
        this.selector = selector;
        this.container = document.querySelector(this.selector);
        this.prevClass = prevClass;
        this.nextClass = nextClass;
        this.prev = document.querySelector(prevClass);
        this.next = document.querySelector(nextClass);
        this.state = 0;
        this.minState = 0;
        this.maxState = this.container?.children.length - 1;

        if (!this.container) return;

        if(mobile){
            if(window.innerWidth <= 991) this.init()
        } else {
            this.init();
        }
    }

    updateUI(state){
        const elements = this.container.children;

        Array.from(elements).forEach(element => {
            element.style.display = 'none';
            element.ariaHidden = 'true'
        });

        this.container.children[state].style.display = 'block';
        this.container.children[state].ariaHidden = 'false';
    }

    updateState(value){
        if(value == '-') {
            if( this.state == this.minState) {
                this.state = this.maxState;
            } else {
                this.state--;
            }

            this.updateUI(this.state);
        }

        if(value == '+') {
            if( this.state == this.maxState) {
                this.state = this.minState;
            }   else {
                this.state++;
            }

            this.updateUI(this.state);
        }
    }

    verifyControls(elem){
        if (elem.classList.contains(this.prevClass.replace('.', ''))) {
            this.updateState('-');
        }

        if (elem.classList.contains(this.nextClass.replace('.', ''))) {
            this.updateState('+')
        }
    }

    trackEvents(elem){
        elem.addEventListener('click', () => {
            this.verifyControls(elem)
        });
    }

    verifyTouchEvents(event){
        if (event[0] > event[1]) {
            this.updateState('+');
        } else if (event[0] < event[1]) {
            this.updateState('-');
        }
    }

    touchEvents(elem){
        let event = [];

        elem.addEventListener('touchstart', (e) => {
            event = [];
            event.push(e.changedTouches[0].clientX);
        })

        elem.addEventListener('touchend', (e) => {
            event.push(e.changedTouches[0].clientX);
            this.verifyTouchEvents(event)
        })
    }

    init(){
        this.trackEvents(this.prev);
        this.trackEvents(this.next);
        this.touchEvents(this.container)
    }
}

let slider = new Slider('.mobile-carousel', '.control-prev', '.control-next', true);


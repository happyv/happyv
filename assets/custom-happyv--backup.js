(function($) {
	$(document).ready(function() {
  
      $('.more-button').click(function(){
          $('.menu--overlay-inner>.nav-button').toggleClass('nav-show');
          $('.menu--overlay').toggleClass('nav-show');
          $('.menu--overlay-inner').toggleClass('nav-show');
          //$('body').toggleClass('nav-show');
      });

      $('.menu--overlay-inner>.nav-button').click(function(){
          $('.menu--overlay').toggleClass('nav-show');
          $('.menu--overlay-inner').toggleClass('nav-show');
          $(this).toggleClass('nav-show');
          //$('body').toggleClass('nav-show');
      });
	
    });
})(jQuery);

function swipe(c_str, rightFunc, leftFunc) {
    
    var container = document.querySelector(c_str);

    container.addEventListener("touchstart", StartTouch, { passive: true });
    container.addEventListener("touchmove", MoveTouch, { passive: true });

    var initialX = null;
    var initialY = null;

    function StartTouch(e) {
      initialX = e.touches[0].clientX;
      initialY = e.touches[0].clientY;
    };

    function MoveTouch(e) {
      if (initialX === null) {
        return;
      }

      if (initialY === null) {
        return;
      }

      var currentX = e.touches[0].clientX;
      var currentY = e.touches[0].clientY;

      var diffX = initialX - currentX;
      var diffY = initialY - currentY;

      if (Math.abs(diffX) > Math.abs(diffY)) {
        if (diffX > 0) {
          rightFunc();
        } else {
          leftFunc();
        }  
      }

      initialX = null;
      initialY = null;

      e.preventDefault();
    };
  }

window.addEventListener("klaviyoForms", function(e) {
  if (e.detail.type == 'submit') {
     $(".form-sect").addClass('yessuc');
  }
});


// index page


    var slideCount = $('.slider ul li').length-1;
    var slideWidth = $('.slider ul li:last-child').width()+30;
    var slideWidth0 = $('.slider ul li:first-child').width();
    var sliderUlWidth = slideCount * slideWidth + slideWidth0;
    var clientWidth = $("body")[0].offsetWidth;

    if (clientWidth > 736) {
      if (clientWidth < 980) {
        var sliderUl2Width = slideWidth + slideWidth0
      }
      else if (clientWidth < 1600) {
        var sliderUl2Width = 2 * slideWidth + slideWidth0
      }
      else 
      {
        var sliderUl2Width = 3 * slideWidth + slideWidth0
      }
    } 
  
    $('.slider').css({ width: sliderUl2Width });
    $('.slider ul').css({ width: sliderUlWidth });
    if(clientWidth < 980){
      $($('.slider ul li img')[0]).css("visibility", "initial");
      $($('.slider ul li img')[1]).css("visibility", "initial");
    }else if(clientWidth < 1600){
      $($('.slider ul li img')[0]).css("visibility", "initial");
      $($('.slider ul li img')[1]).css("visibility", "initial");    
      $($('.slider ul li img')[2]).css("visibility", "initial");
    }else {
      $($('.slider ul li img')[0]).css("visibility", "initial");
      $($('.slider ul li img')[1]).css("visibility", "initial");
      $($('.slider ul li img')[2]).css("visibility", "initial");    
      $($('.slider ul li img')[3]).css("visibility", "initial");
    }

    var i=1;
    $('.testimonal--text div:nth-child('+ i +')').addClass("show");

    function moveLeft2() {
      $('.slider ul').animate({
          left: + slideWidth
      }, 200, function () {
          $('.slider ul li:last-child').prependTo('.slider ul');
          $($('.slider ul li img')[0]).css("visibility", "initial");
          $('.slider ul').css('left', '');
      });
      $('.testimonal--text div:nth-child('+ i +')').removeClass("show");i=i-1;
      if (i < 1) {i = $('.slider ul li').length}
      $('.testimonal--text div:nth-child('+ i +')').addClass("show");
    }

    function moveRight2() {
      $('.slider ul').animate({
          left: - slideWidth
      }, 200, function () {
          $('.slider ul li:first-child').appendTo('.slider ul');
          if(clientWidth < 980){
            $($('.slider ul li img')[1]).css("visibility", "initial");
          }else if(clientWidth < 1600){
            $($('.slider ul li img')[2]).css("visibility", "initial");
          }else {
            $($('.slider ul li img')[3]).css("visibility", "initial");
          }
          $('.slider ul').css('left', '');
      });
      $('.testimonal--text div:nth-child('+ i +')').removeClass("show");
      i=i+1;
      if (i > $('.slider ul li').length) {i = 1}
      $('.testimonal--text div:nth-child('+ i +')').addClass("show");
    }

    $('.slider a.control-prev').click(function () {
      moveLeft2();
    });

    $('.slider a.control-next').click(function () {
      moveRight2();
    }); 
  
  	if (document.querySelector(".hp--testimonials-slider")) {swipe(".hp--testimonials-slider", moveRight2, moveLeft2);}
  
    $('.slider ul li:first-child img').on('load', function(){
      var slideHeight = $('.slider ul li:first-child img').height();
      $('.slider ul').css({ height: slideHeight });
    });
  
   $('.hp--experts a.control-next').click(function(){
    expertLeftHandler();
  });

  $('.hp--experts a.control-prev').click(function(){
    expertRightHandler();
  });
  
  var expertCount = $(".hp--experts .expert-item").length;
  var expertCurr = 1;

  function expertLeftHandler(){
    $(".expert-item").removeClass("show");
    expertCurr++;
    if (expertCurr > expertCount) { expertCurr = 1; }
    expertCurrName = ".expert-item-"+expertCurr;
    $(expertCurrName).addClass("show");
  }

  function expertRightHandler(){
    $(".expert-item").removeClass("show");
    expertCurr--;
    if (expertCurr < 1) { expertCurr = expertCount; }
    expertCurrName = ".expert-item-"+expertCurr;
    $(expertCurrName).addClass("show");
  }
    
  if (document.querySelector(".hp--experts")) {swipe(".hp--experts", expertLeftHandler, expertRightHandler);}
var pageWidth = $('.hp--blog').width();
if (pageWidth < 736 ) {
pageWidth = pageWidth*0.40;  
$('.hp--blog ul').scrollLeft( pageWidth );
}

/* customerTemplates */

window.theme = theme || {};

theme.customerTemplates = (function() {
  var selectors = {
    RecoverHeading: '#RecoverHeading',
    RecoverEmail: '#RecoverEmail',
    LoginHeading: '#LoginHeading'
  };

  function initEventListeners() {
    this.$RecoverHeading = $(selectors.RecoverHeading);
    this.$RecoverEmail = $(selectors.RecoverEmail);
    this.$LoginHeading = $(selectors.LoginHeading);

    // Show reset password form
    $('#RecoverPassword').on(
      'click',
      function(evt) {
        console.log("-------");
        evt.preventDefault();
        showRecoverPasswordForm();
        this.$RecoverHeading.attr('tabindex', '-1').focus();
      }.bind(this)
    );

    // Hide reset password form
    $('#HideRecoverPasswordLink').on(
      'click',
      function(evt) {
        evt.preventDefault();
        hideRecoverPasswordForm();
        this.$LoginHeading.attr('tabindex', '-1').focus();
      }.bind(this)
    );

    this.$RecoverHeading.on('blur', function() {
      $(this).removeAttr('tabindex');
    });

    this.$LoginHeading.on('blur', function() {
      $(this).removeAttr('tabindex');
    });

    isMenuActive();

    $(document).on('click', '.account-menu li a', function() {
      setTimeout(function() {
        isMenuActive();
      }, 100);
    })

    function isMenuActive() {
      var href = document.location.href;
      if(href.indexOf('myAccountProfile') > -1) {
        $('.account-link--profile').addClass('active');
        $('.account-link--rewards').removeClass('active');
      }
      else if(href.indexOf('rewards') > -1) {
        $('.account-link--profile').removeClass('active');
        $('.account-link--rewards').addClass('active');
      }
      else {
        $('.account-link--profile').removeClass('active');
        $('.account-link--rewards').removeClass('active');
      }
    }
    
      
    $('.password-toggle').on('click', function() {
   	  var $input = $(this).closest('.form-field-wrapper').find('input');
      
      $(this).toggleClass('active');
      var type = 'text';
      if($input.attr('type') != 'text') {
        type = 'text';
      }
      else {
        type = 'password';
      }
      $input.attr('type', type);
    });
    
        
    $('.btn-create-account').on('click', function(e) {
      e.preventDefault();
      var $form = $(this).closest('form');
      var $psd = $form.find('#RegisterForm-password');
      var $psd_confirm = $form.find('#RegisterForm-password-confirm');
      var $email = $form.find('#RegisterForm-email');
      
      var psd = $psd.val();
      var psd_confirm = $psd_confirm.val();
      var email = $email.val();
      
      var $msg__psd_blank = $form.find('.input-error-message.psd-blank');
      var $msg__psd_short = $form.find('.input-error-message.psd-short');
      var $msg__psd_diff = $form.find('.input-error-message.psd-diff')
      var $msg__email_blank = $form.find('.input-error-message.email-blank');
      var $msg__email_invalid = $form.find('.input-error-message.email-invalid');

      var $field_fname = $form.find('#RegisterForm-FirstName');
      var $field_lname = $form.find('#RegisterForm-LastName');

      if($field_fname.val() == '') {
        $field_fname.focus();
        return false;
      }

      if($field_lname.val() == '') {
        $field_lname.focus();
        return false;
      }

      if(psd != psd_confirm) {
        $msg__psd_blank.addClass('hide');
        $msg__psd_short.addClass('hide');
        $msg__psd_diff.removeClass('hide');
        return false;
      }
      else {
        $msg__psd_diff.addClass('hide');
        if(psd == '') {
          $msg__psd_blank.removeClass('hide');
          $msg__psd_short.addClass('hide');
          return false;
        }
        else if(psd.length < 5) {
          $msg__psd_blank.addClass('hide');
          $msg__psd_short.removeClass('hide');
          return false;
        }
      }
      
      var email_seg = email.split('@');
      
      if(email == '') {
        $msg__email_blank.removeClass('hide');
        $msg__email_invalid.addClass('hide');
        return false;
      }
      else {
        if(email_seg.length < 2) {
          $msg__email_blank.addClass('hide');
          $msg__email_invalid.removeClass('hide');
          return false;
        }
        else {
          if(email_seg[0] == '' || email_seg[1] == '') {
            $msg__email_blank.addClass('hide');
            $msg__email_invalid.removeClass('hide');
            return false;
          }
          else {
            var email_dot_seg = email_seg[1].split('.');
            if(email_dot_seg.length < 2) {
              $msg__email_blank.addClass('hide');
              $msg__email_invalid.removeClass('hide');
              return false;
            }
            else {
              if(email_dot_seg[0] == '' || email_dot_seg[1] == '') {
                $msg__email_blank.addClass('hide');
                $msg__email_invalid.removeClass('hide');
                return false;
              }
            }
          }
        }
      }

      $form.submit();
    })

  }

  /**
   *
   *  Show/Hide recover password form
   *
   */

  function showRecoverPasswordForm() {
    $('#RecoverPasswordForm').removeClass('hide');
    $('#CustomerLoginForm').addClass('hide');

    if (this.$RecoverEmail.attr('aria-invalid') === 'true') {
      this.$RecoverEmail.focus();
    }
  }

  function hideRecoverPasswordForm() {
    $('#RecoverPasswordForm').addClass('hide');
    $('#CustomerLoginForm').removeClass('hide');
  }

  /**
   *
   *  Show reset password success message
   *
   */
  function resetPasswordSuccess() {
    var $formState = $('.reset-password-success');

    // check if reset password form was successfully submited.
    if (!$formState.length) {
      return;
    }

    // show success message
    $('#ResetSuccess')
      .removeClass('hide')
      .focus();
  }

  /**
   *
   *  Show/hide customer address forms
   *
   */
  function customerAddressForm() {
    var $newAddressForm = $('#AddressNewForm');
    var $newAddressFormButton = $('#AddressNewButton');

    if (!$newAddressForm.length) {
      return;
    }

    // Initialize observers on address selectors, defined in shopify_common.js
    if (Shopify) {
      // eslint-disable-next-line no-new
      new Shopify.CountryProvinceSelector(
        'AddressCountryNew',
        'AddressProvinceNew',
        {
          hideElement: 'AddressProvinceContainerNew'
        }
      );
    }

    // Initialize each edit form's country/province selector
    $('.address-country-option').each(function() {
      var formId = $(this).data('form-id');
      var countrySelector = 'AddressCountry_' + formId;
      var provinceSelector = 'AddressProvince_' + formId;
      var containerSelector = 'AddressProvinceContainer_' + formId;

      // eslint-disable-next-line no-new
      new Shopify.CountryProvinceSelector(countrySelector, provinceSelector, {
        hideElement: containerSelector
      });
    });

    // Toggle new/edit address forms
    $('.address-new-toggle').on('click', function() {
      var isExpanded = $newAddressFormButton.attr('aria-expanded') === 'true';
      
      $('.address-grid-wrapper').toggleClass('hide');

      $newAddressForm.toggleClass('hide');
      $newAddressFormButton.attr('aria-expanded', !isExpanded).focus();
    });

    $('.address-edit-toggle').on('click', function() {
      var formId = $(this).data('form-id');
      var $editButton = $('#EditFormButton_' + formId);
      var $editAddress = $('#EditAddress_' + formId);
      var isExpanded = $editButton.attr('aria-expanded') === 'true';
		
      $('.address-edit-form-header').toggleClass('hide');
      $('.address-grid-wrapper').toggleClass('w-full');
      
      $(".address-grid-wrapper").toggleClass('hide');
      $(this).closest('.address-grid-wrapper').removeClass('hide');
      
      $editAddress.toggleClass('hide');
      $editButton.attr('aria-expanded', !isExpanded).focus();
    });

    $('.address-delete').on('click', function() {
      var $el = $(this);
      var formId = $el.data('form-id');
      var confirmMessage = $el.data('confirm-message');

      // eslint-disable-next-line no-alert
      if (
        confirm(
          confirmMessage || 'Are you sure you wish to delete this address?'
        )
      ) {
        Shopify.postLink('/account/addresses/' + formId, {
          parameters: { _method: 'delete' }
        });
      }
    });
    

    
  }

  /**
   *
   *  Check URL for reset password hash
   *
   */
  function checkUrlHash() {
    var hash = window.location.hash;

    // Allow deep linking to recover password form
    if (hash === '#recover') {
      showRecoverPasswordForm.bind(this)();
    }
  }

  return {
    init: function() {
      initEventListeners();
      checkUrlHash();
      resetPasswordSuccess();
      customerAddressForm();
    }
  };
})();


theme.init = function() {
  theme.customerTemplates.init();
  };

$(theme.init);
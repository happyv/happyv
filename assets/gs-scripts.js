/*
*GS scripts
*
*/

(() => {
    const weekdays = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ]

    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ]

    const date = new Date()

    const serializeDateTime = date => ({
        year: date.getFullYear(),
        month: date.getMonth(),
        day: date.getDate(),
        weekday: date.getDay(),
        hours: date.getHours(),
        minutes: date.getMinutes(),
        seconds: date.getSeconds(),
        miliseconds: date.getMilliseconds()
    });

    const dateResults = serializeDateTime(date);

    const dayOne = new Date(
        dateResults.year,
        dateResults.month,
        dateResults.day + 1,
        dateResults.hours,
        dateResults.minutes,
        dateResults.seconds,
        dateResults.miliseconds
    );

    const dayTwo = new Date(
        dateResults.year,
        dateResults.month,
        dateResults.day + 3,
        dateResults.hours,
        dateResults.minutes,
        dateResults.seconds,
        dateResults.miliseconds
    );

    const getOrdinalNumber = function(d) {
      if (d > 3 && d < 21) return 'th';

      switch (d % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
      }
    }

    //estimate shipping date
    const setEstimation = () => {
        const dateResults1 = serializeDateTime(dayOne);
        const dateResults2 = serializeDateTime(dayTwo);
        const estimation = theme.strings.dateEstimation;

        const finalEstimation = estimation
            .replace('{{date1}}', `<strong> ${weekdays[dateResults1.weekday]}, ${months[dateResults1.month]} ${dateResults1.day + getOrdinalNumber(dateResults1.day)}</strong>`)
            .replace('{{date2}}', `<strong> ${weekdays[dateResults2.weekday]}, ${months[dateResults2.month]} ${dateResults2.day + getOrdinalNumber(dateResults2.day)}</strong>`);

        const selector = document.querySelector('.gs-time_estimator');
        if (selector) selector.innerHTML = finalEstimation;
    }

    //Insert svg icons from shopify api into rebuy app (cart)
    const getSvgIcons = () => {
        const selector = document.querySelectorAll('.svg-icons__hidden svg');
        const cart = document.querySelector('.menu--cart');
        const addToCart = document.querySelectorAll('button');
        const inputBtn = document.querySelectorAll('input');

        const getSvg = () => {
            const cartDrawwer =  document.querySelector('.payment-icons__list');
            const verifyIcons = document.querySelectorAll('.payment-icons__list svg');
            // Stop if cartDrawwer isn't found to prevent JS errors
            if (!cartDrawwer) return;
            if(verifyIcons.length == 0) {
                Array.from(selector).forEach( (item) => {
                    cartDrawwer.appendChild(item)
                })
            }
        }

        const addToCartForm = selector => {
            Array.from(selector).forEach( (item) => {
                const itemId = item.id;

                itemId.includes("add-to-cart-top")
                    && item.addEventListener('click', () => {
                        setTimeout(()=> {
                            getSvg()
                    }, 1000)
                })
            })
        }

        const addEventListenersCart = _ => {
            addToCartForm(addToCart);

            Array.from(inputBtn).forEach( (item) => {
                item.addEventListener('click', () => {
                    setTimeout(()=> {
                        getSvg()
                    }, 800)
                })
            })

            const btn = document.querySelector('button.product-form__cart-submit');
            btn && btn.addEventListener('click', () => {
                getSvg();
            });

            cart.addEventListener('click', getSvg);
        }

        addEventListenersCart();
    }

    const addBracketsToReviews = _ => {

        const reviewSelectors = document.querySelectorAll('.gs-reviews__selector .jdgm-prev-badge__text');
        console.log(reviewSelectors)

        reviewSelectors
            && Array.from(reviewSelectors).forEach((sel) => {
                const defaultText = sel.innerText;
                const newText = '(' + defaultText
                    .replace(' reviews', '')
                    .replace(' review', '')
                    .replace('No', '0') + ')';

                setTimeout( () => {
                    sel.innerText = newText;
                    const changeOpacity = document.querySelectorAll(".gs-reviews__selector .jdgm-widget.jdgm-widget");
                    Array.from(changeOpacity).forEach((element) => {
                        element.style.opacity = 1;
                    })
                }, 70)
            });
    }

    addBracketsToReviews()
    setEstimation()
    getSvgIcons()
})()

function addItemToCart(variant_id, qty, selling_plan) {
    var cartCount = '{{ cart.item_count }}';
    data = {
      "id": variant_id,
      "quantity": qty,
      "selling_plan": selling_plan
    }
    jQuery.ajax({
      type: 'POST',
      url: '/cart/add.js',
      data: data,
      dataType: 'json',
      success: function() { 
        window.SLIDECART_UPDATE();
      }
    });
    window.SLIDECART_OPEN();
  }

$('.faq-more').click(function(){
  $('.p-faq').toggleClass('show');
});
    
$('.product-form__cart-submit').click(function(){
  if($('.rc_block__type__autodeliver').hasClass('rc_block__type--active')) {
    $('.product-form__input').val(1).change();
  }
});

var slideCountGal, slideWidthGal, slideHeightGal, sliderUlWidthGal, clientWidth;
  (function($) {
	$($('.p-image-wrap img')[0]).css("display","initial");
    slideCountGal = $('.slider-gallery li').length;
    slideWidthGal = $('.slider-gallery li').width();
    slideHeightGal = $('.slider-gallery li img').height();
    sliderUlWidthGal = (slideCountGal+1) * slideWidthGal;
    clientWidth = $("body")[0].offsetWidth;

    $('.slider-gallery').css({ height: slideHeightGal });
    $('.slider-gallery ul').css({ width: sliderUlWidthGal });
    $('.slider-gallery li:last-child').prependTo('.slider-gallery ul');
    $('.slider-gallery li:first-child img').css("visibility", "initial");
    if(clientWidth > 710) $('.slider-gallery li img').css("visibility", "initial");
    
  })(jQuery);
  
  // <--   Stats Items Control Start  -->
   $('.p-i-stats--items a.control-next').click(function(){
    statLeftHandler();
  });

  $('.p-i-stats--items a.control-prev').click(function(){
    statRightHandler();
  });
  
  var statCount = $(".p-i-stats--items .stat-item").length;
  var statCurr = 1;

  function statLeftHandler(){
    $(".stat-item").removeClass("show");
    statCurr++;
    if (statCurr > statCount) { statCurr = 1; }
    statCurrName = ".stat-item-"+statCurr;
    $(statCurrName).addClass("show");
    draw_circle_diagram();
  }

  function statRightHandler(){
    $(".stat-item").removeClass("show");
    statCurr--;
    if (statCurr < 1) { statCurr = statCount; }
    statCurrName = ".stat-item-"+statCurr;
    $(statCurrName).addClass("show");
    draw_circle_diagram();
  }
    
  if (document.querySelector(".p-i-stats--items")) {swipe(".p-i-stats--items", statLeftHandler, statRightHandler);}
  
  // <--   Stats Items Control End  -->
    
  // <--   Doctors Items Control Start  -->
    
  var expertCount=0;
  var expertNmb = $('.p-doctors').find('.experts-content').length-1;
  $('.p-doctors a.control-next').click(function(){
    doctorLeftHandler();
  });

  $('.p-doctors a.control-prev').click(function(){
    doctorRightHandler();
  });
    
  function doctorLeftHandler(){
    expertCount++;
    if(expertCount > expertNmb) {expertCount = 0;}
    $('.experts-img').removeClass('show');
    $('.experts-content').removeClass('show');
    $('.experts-img-'+expertCount).addClass('show');
    $('.experts-content-'+expertCount).addClass('show');
  }

  function doctorRightHandler(){
    expertCount--;
    if(expertCount < 0) {expertCount = expertNmb;}
    $('.experts-img').removeClass('show');
    $('.experts-content').removeClass('show');
    $('.experts-img-'+expertCount).addClass('show');
    $('.experts-content-'+expertCount).addClass('show');
  }
    
  if (document.querySelector(".p-doctors")) {swipe(".p-doctors .p-content", doctorLeftHandler, doctorRightHandler);}

  // <--   Doctors Items Control End  -->
    
  // <--   Slider Gallery Control Start  -->


  function moveLeftGal() {
      $('.slider-gallery ul').animate({
          left: + slideWidthGal}, 200, function () {
          $('.slider-gallery li:last-child').prependTo('.slider-gallery ul');
          if(clientWidth < 710) $($('.slider-gallery li img')[0]).css("visibility", "initial");
          $('.slider-gallery ul').css('left', '');
      });
  }

  function moveRightGal() {
      $('.slider-gallery ul').animate({
          left: - slideWidthGal}, 200, function () {
          $('.slider-gallery li:first-child').appendTo('.slider-gallery ul');
          if(clientWidth < 710) $($('.slider-gallery li img')[0]).css("visibility", "initial");
          $('.slider-gallery ul').css('left', '');
      });
  }

  $('.slider-gallery a.control-prev').click(function () {
      moveLeftGal();
  });

  $('.slider-gallery a.control-next').click(function () {
      moveRightGal();
  });
    
  if (document.querySelector(".slider-gallery")) { swipe(".slider-gallery", moveRightGal, moveLeftGal); }
  
  // <--   Slider Gallery Items Control End  -->

  var transp = $('.p-transparency .g--2	').css('display'); 
  $('.p-transparency .tab-header').click(function(){
    if($(this).parent('.p-tabs-item').hasClass('open') == false) {
      var nmb = $(this).parent('.p-tabs-item').attr('id');
      nmb = nmb +'-img';
      var elem = document.getElementById('nmb');
      $('.p-transparency .g--2 .img-wrap img').removeClass('show');
      $('#'+nmb).addClass('show');
    };
    $(this).parent('.p-tabs-item').toggleClass('open');
  });
  
  function polar_to_cartesian(centerX, centerY, radius, angle_in_degrees) {
    var angle_in_radians = (angle_in_degrees-90) * Math.PI / 180.0;

    return {
      x: centerX + (radius * Math.cos(angle_in_radians)),
      y: centerY + (radius * Math.sin(angle_in_radians))
    };
  }

  function describe_arc(x, y, radius, start_angle, end_angle){
      var start = polar_to_cartesian(x, y, radius, end_angle);
      var end = polar_to_cartesian(x, y, radius, start_angle);
      var arc_sweep = end_angle - start_angle <= 180 ? "0" : "1";
      var d = [
          "M", start.x, start.y, 
          "A", radius, radius, 0, arc_sweep, 0, end.x, end.y
      ].join(" ");

      return d;
  }

  function draw_circle_diagram(){

    var $circle_diagrams = document.getElementsByClassName('circle-diagram');

    for (var $i = 0; $i < $circle_diagrams.length; $i++){
      var $svg_width =    $circle_diagrams[$i].clientWidth;
      var $svg_height =   $circle_diagrams[$i].clientHeight;
      var $stroke_color = $circle_diagrams[$i].getAttribute("data-stroke-color");
      var $stroke_width = $circle_diagrams[$i].getAttribute("data-stroke-width");
      var $radius =       parseInt(($svg_height/2) - ($stroke_width / 2),10);
      var $path =         $circle_diagrams[$i].querySelector('.circle-diagram__arc');
      var $percent =      $circle_diagrams[$i].getAttribute("data-percent");

      if($percent < 0) 
        $percent = 0;
      if($percent < 100 && $percent >= 0) 
        $path.setAttribute("d", describe_arc($svg_width/2, $svg_height/2, ($svg_height/2) - ($stroke_width / 2), 0, (360*$percent)/100));
      if($percent >= 100) {
        $path.setAttribute("d", "M "+$svg_width/2+", "+$svg_height/2+" m -"+$radius+", 0 a "+$radius+","+$radius+" 0 1,0 "+$radius*2+",0 a "+$radius+","+$radius+" 0 1,0 -"+$radius*2+",0"); // это выдаст окружность
        $percent = 100;
      }

      $path.setAttribute("stroke", $stroke_color);
      $path.setAttribute("stroke-width", $stroke_width);

    }
  }

  draw_circle_diagram();
  
  
   
  // <!--   product header lazy load image gallery -->
      
  function gallerySelect(i){
      $('.p-image-wrap img').css("display", "none")
      var img = $("img[data-index="+ i +"]");
      img.css("display","initial");
  }
    
  // <!--   Magic Scroll -->
  
  var controller = new ScrollMagic.Controller();
  window.addEventListener('DOMContentLoaded', resize, {passive: true});
  window.addEventListener('load', resize, {passive: true});
  window.addEventListener('resize', resize, {passive: true});
  window.addEventListener('orientationchange', resize, {passive: true});

  function resize()
  {
    var top = $(".number")[0].offsetHeight + $(".number")[0].offsetTop;
    var left = $(".number")[0].offsetLeft + $(".number")[0].offsetWidth / 2;

    $('.line').css({
          'top': top + 'px',
        'left': left + 'px',
        'height': '0%',
          'z-index' : '0'
    });

    if(parseInt(document.body.clientWidth) > 960) {

      controller = controller.destroy(true);
      controller = new ScrollMagic.Controller();

      var tl = new TimelineMax();  
      var tl2 = new TimelineMax();      
      var tl3 = new TimelineMax();      
      var tl4 = new TimelineMax();

      $(".timeline-step").each(function(i) {
        tl.to(this, 1, {opacity: 1});
      });
      const scene = new ScrollMagic.Scene({
        triggerElement: ".time-line-container",
        triggerHook: "onCenter",
        duration: "110%"
      })
      .setTween(tl)
      .addTo(controller);

      $(".line").each(function(index) {
        if(index < $(".line").length - 1) tl4.to(this, 1, {height: "100%"});
      });
      const scene4 = new ScrollMagic.Scene({
        triggerElement: ".time-line-container",
        triggerHook: "onCenter",
        duration: "110%"
      })
      .setTween(tl4)
      .addTo(controller);

      tl2.to(".time-line-container .g--2", 1, {yPercent: 0});
      const scene1 = new ScrollMagic.Scene({
        triggerElement: "#HowItWorks",
        triggerHook: "onLeave",
        duration: "70%"
      })
      .setPin(".time-line-container .g--2")
      .setTween(tl2)
      .addTo(controller);

    }else{

      controller = controller.destroy(true);
      controller = new ScrollMagic.Controller();
      $(".time-line-container ul").css("position","relative");        
      $(".number").css("margin-top", "0");
      var top = $(".number")[0].offsetHeight + $(".number")[0].offsetTop;
      var left = $(".number")[0].offsetLeft + $(".number")[0].offsetWidth / 2;

      $('.line').css({
          'top': top + 'px',
        'left': left + 'px',
        'height': '0%',
          'z-index' : '0'
      });

      var tl = new TimelineMax();  
      var tl2 = new TimelineMax();

        $(".timeline-step").each(function(i) {
          tl.to(this, 1, {opacity: 1, ease: Power4.easeOut}, "-=0.3"); 
        });

        const scene = new ScrollMagic.Scene({
          triggerElement: ".time-line-container",
          triggerHook: "onLeave",
          duration: "130%"
        })
        .setTween(tl)
        .addTo(controller);


          $(".line").each(function(index) {
          if(index < $(".line").length - 1) tl2.to(this, 1, {height: "100%"}, "-=0.3"); 
        });

        const scene1 = new ScrollMagic.Scene({
          triggerElement: ".time-line-container .g--1",
          triggerHook: "onCenter",
          duration: "180%"
        })
        .setTween(tl2)
        .addTo(controller);

    }
  }

  $('.slider-gallery li img').on('load', function(){
      var slideHeightGal = $('.slider-gallery li img').height();
      var sliderUlWidthGal = (slideCountGal+1) * slideWidthGal;

      $('.slider-gallery').css({ height: slideHeightGal });
      $('.slider-gallery ul').css({ width: sliderUlWidthGal });
  });
                     
$('.p-nav--monthly').click(function() {
                       $('.p-nav--price').removeClass('show');
                       $('.p-nav--price-monthly').addClass('show');
                       $('.p-nav--option').removeClass('active');
                       $(this).addClass('active');
                       $('.menu-wrap--inner .btn').removeClass('show');
                       $('.page-nav--link').addClass('show');
                       
});
                       
$('.p-nav--single').click(function() {
                       $('.p-nav--price').removeClass('show');
                       $('.p-nav--price-single').addClass('show');
                       $('.p-nav--option').removeClass('active');
                       $(this).addClass('active');
                       $('.menu-wrap--inner .btn').removeClass('show');
                       $('.page-nav--btn').addClass('show');
});
                       
                       $('.p-nav--quaterly').click(function() {
                       $('.p-nav--price').removeClass('show');
                       $('.p-nav--price-quaterly').addClass('show');
                       $('.p-nav--option').removeClass('active');
                       $(this).addClass('active');
                       $('.menu-wrap--inner .btn').removeClass('show');
                       $('.page-nav--link3').addClass('show');
});

// <--   Bundle Upsale Control Start  -->
  
  var bundleCount = $(".bundle-upsale-wrap .bundle-upsale").length;
  var bundleCurr = 1;
  
  function bundleLeftHandler(){
    $(".bundle-upsale").removeClass("show");
    bundleCurr++;
    if (bundleCurr > bundleCount) { bundleCurr = 1; }
    bundleCurrName = ".upsale-item-"+bundleCurr;
    $(bundleCurrName).addClass("show");
  }
  
  function bundleRightHandler(){
    $(".bundle-upsale").removeClass("show");
    bundleCurr--;
    if (bundleCurr < 1) { bundleCurr = bundleCount; }
    bundleCurrName = ".upsale-item-"+bundleCurr;
    $(bundleCurrName).addClass("show");
  }

  $(".bundle-upsale-wrap .left-arr").click(function(){
    bundleLeftHandler()
  });
  
  $(".bundle-upsale-wrap .right-arr").click(function(){
    bundleRightHandler()
  });
  
  if (document.querySelector(".bundle-upsale-wrap")) { swipe(".bundle-upsale-wrap", bundleLeftHandler, bundleRightHandler);}
  
  // <--   Bundle Upsale Control End  -->

  var itemScore = $('.jdgm-prev-badge__stars').attr('data-score');
  var itemScoreN = parseFloat(itemScore);
  switch (itemScoreN) {
    case 5:
      break;
    case 4:
      $('.jdgm-prev-badge__stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--off');
      $('.jdgm-rev-widg__summary-stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--off');      
      break;
    case 4.5:
      $('.jdgm-prev-badge__stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--half');
      $('.jdgm-rev-widg__summary-stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--half');      
      break;
    default: 
      if(itemScoreN > 4.5) {
        $('.jdgm-prev-badge__stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--more');
        $('.jdgm-rev-widg__summary-stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--more');
      } else if (itemScoreN > 4) {
        $('.jdgm-prev-badge__stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--less');
        $('.jdgm-rev-widg__summary-stars .jdgm-star:nth-child(5)').removeClass('jdgm--on').addClass('jdgm--less');
      }
      break;
  }